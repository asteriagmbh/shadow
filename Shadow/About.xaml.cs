﻿using System;
using System.Windows;

namespace Shadow
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();
            aboutDocument.DataContext = new AboutDataModel { CopyrightDate = DateTime.Now.Year.ToString() };
        }

    }
}
