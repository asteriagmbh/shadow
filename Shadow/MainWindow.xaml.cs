﻿using Shadow;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Shadow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Brush defaultBackground;
        private readonly List<WtsUserSession> _sessions = new List<WtsUserSession>();
        public MainWindow()
        {
            InitializeComponent();
            UpdateSessions();

            defaultBackground = statusbar.Background;

        }

        private async void UpdateSessions()
        {
            lblState.Text = "Loading...";
            var hostname = txtHostName.Text;
            var _sessions = await Task.Run(() => TSManager.ListUsers(hostname));

            SessionsList.ItemsSource = _sessions.OrderBy(s => s.UserName);
            lblState.Text = "Ready.";

        }

        void Button_Click(object sender, RoutedEventArgs e)
        {
            UpdateSessions();
        }

        void SessionsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // mstsc /v:COMPUTERNAME oder IP - ADRESSE /shadow:1 /control

            var currentSession = ((FrameworkElement)e.OriginalSource).DataContext as WtsUserSession;

            if (currentSession == null)
                return;

            if (controlRemoteSession.IsChecked == true)
            {
                Process.Start("mstsc.exe", $"/v:{txtHostName.Text} /shadow:{currentSession.SessionId} /control");
            }
            else
            {
                Process.Start("mstsc.exe", $"/v:{txtHostName.Text} /shadow:{currentSession.SessionId}");
            }
        }

        private void Link_Click(object sender, RoutedEventArgs e)
        {
            new About().ShowDialog();
        }

        private void ControlRemoteSession_Checked(object sender, RoutedEventArgs e)
        {
            statusbar.Background = Brushes.IndianRed;
        }

        private void ControlRemoteSession_Unchecked(object sender, RoutedEventArgs e)
        {
            statusbar.Background = defaultBackground;

        }
    }
}
