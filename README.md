# Shadow

Shadow lets you remote control a Remote Desktop session. 

It lists available sessions and then uses the Microsoft Remote Desktop Connection tool mstsc.exe to open a remote session.

![picture](Shadow.PNG)

## Requirements
- Windows Server 2012 R2 or higher is required.
- Remote Control is only possible if the group policy setting **Administrative Templates\Windows Components\Terminal Services\Sets rules for remote control of Terminal Services user sessions** is set to one of the following values:
  - Not configured
  - Disabled
  - Full Control with user's permission
  - Full Control without user's permission

## Privacy notice
Viewing or controlling a screen/session without user's permission is not good practice. It usually violates privacy laws and or company policies. 

Doing so without user's consent might result in you being sacked and/or prosecuted. So ask before looking.

## How to Use

1. Run **shadow.exe** with administrative privileges.
2. Enter Hostname of RD Session Host.
3. Click on **Load Sessions**.
4. Double-click on one of the sessions to remote control the session.

![picture](OpenSession.PNG)
